INSERT INTO public."Parking_type"(
            type, count_per_hour)
    VALUES 
	('LARGE', 5.0),
	('SIMPLE', 3.0),
	('COVERED', 7.0);
INSERT INTO public."Parking_space"(
             type)
    VALUES 
	('COVERED'),
	('COVERED'),
	('COVERED'),
	('COVERED'),
	('COVERED'),
	('SIMPLE'),
	('SIMPLE'),
	('SIMPLE'),
	('SIMPLE'),
	('SIMPLE'),
	('LARGE'),
	('LARGE'),
	('LARGE'),
	('SIMPLE'),
	('SIMPLE'),
	('SIMPLE'),
	('SIMPLE'),
	('SIMPLE'),
	('SIMPLE'),
	('SIMPLE'),
	('SIMPLE'),
	('SIMPLE'),
	('SIMPLE'),
	('COVERED'),
	('COVERED'),
	('COVERED');

INSERT INTO public."Position"(
            title, salary)
    VALUES 
	('Manager', '2000'),
	('rg3', '1000'),
	('rg2', '800'),
	('rg1', '500');

INSERT INTO public."Employee"(
             first_name, last_name, "position")
    VALUES 
	('Georg','Pipps', 'rg1'),
	('Carlos', 'Gonzelez','rg2'),
	('Fran', 'Wilson' ,'Manager'),
	('Alexander',' Feuer','rg1'),
	('Yvonne',' Moncada','rg3');

INSERT INTO public."Model"(
            body_type, manufacter, model)
    VALUES 
	('fastback', 'Audi', 'A7'),
	('sedan', 'Audi', '100'),
	('sedan', 'Ford', 'Mondeo'),
	('SUV', 'Toyota', 'RAV4'),
	('hatchback', 'Toyota', 'Yaris'),
	('sedan', 'Toyota', 'Camry'),
	('hatchback', 'Kia', 'Rio'),
	('estate', 'Skoda', 'Oktavia III'),
	('estate', 'Renault', 'Megane III'),
	('hatchback', 'Renault', 'Sandero');

INSERT INTO public."Car"(
            "number", model_id)
    VALUES 
	('ab657c', 2),
	('xv112d', 10),
	('pr543t', 8),
	('as123n', 6),
	('as627m', 9),
	('me154c', 8),
	('ab657n', 7),
	('nb600f', 4),
	('gb609d', 8),
	('fb151e', 7),
	('tb352h', 2),
	('fb603t', 8),
	('bb353c', 9),
	('bf454y', 3),
	('qb175y', 5),
	('er156r', 10);

INSERT INTO public."Client"(
             first_name, last_name, phone, car_number)
    VALUES 
	('Maria', 'Anders','12345678901' ,'ab657c'),
	('Ana', 'Trujillo','12345468801' ,'xv112d'),
	('Antonio' ,'Moreno','12345645881' ,'pr543t'),
	('Thomas','Hardy','12345678954' ,'as123n'),
	('Christina','Berglund','12346674901' ,'as627m'),
	('Hanari', 'Carnes','12345673461' ,'me154c'),
	('Mario', 'Pontes','12343456774' ,'ab657n'),
	('Philip', 'Cramer','12344466901' ,'nb600f'),
	('Howard', 'Snyder','12349845901' ,'gb609d'),
	('Mario', 'Pontes','12345673321' ,'fb151e'),
	('Carlos', 'Hernбndez','13354278901' ,'tb352h'),
	('Yoshi', 'Latimer','12267578901' ,'fb603t'),
	('Philip', 'Cramer','12355678901' ,'bb353c'),
	('Yoshi', 'Tannamuri','12345628901' ,'bf454y'),
	('John', 'Steel','12345672241' ,'qb175y'),
	('Renate', 'Messner','12374578901' ,'er156r');

INSERT INTO public."Deal"(
             client_id, parking_number, emplyee_id)
    VALUES 
	(3, 1, 1),
	(1, 14, 4),
	(2, 13, 4),
	(4,9 , 3),
	(5, 17,3),
	(6 , 21,2),
	(7 ,4 ,3),
	(8 , 7,2),
	(9, 21,4),
	(10, 15,5),
	(11,14 ,3),
	(3, 7, 1),
	(15, 1, 1),
	(6,17 ,2),
	(7, 8,5),
	(2, 9, 1),
	(9, 10,3),
	(8, 6,3),
	(5, 8,2),
	(10, 15, 1);

INSERT INTO public."Deal_details"(
             deal_id ,booking_hours, begining_date)
    VALUES 
    	(1, 5, '2016-01-16 12:10:00'),
    	(2, 4, '2016-02-01 17:17:25'),
    	(3, 7, '2016-04-20 17:17:25'),
    	(4, 24, '2016-01-15 17:17:25'),
    	(5, 35, '2016-02-22 17:17:25'),
    	(6, 1, '2016-03-22 17:17:25'),
    	(7, 3, '2016-04-05 17:17:25'),
    	(8, 64, '2016-03-22 17:17:25'),
    	(9, 3, '2016-04-27 17:17:25'),
    	(10, 9, '2016-03-05 17:17:25'),
    	(11, 3, '2016-01-27 17:17:25'),
    	(12, 1, '2016-02-27 17:17:25' ),
    	(13, 1, '2016-05-05 17:17:25'),
    	(14, 2, '2016-01-03 17:17:25'),
    	(15, 2, '2016-01-17 17:17:25'),
    	(16, 1, '2016-03-07 17:17:25'),
    	(17, 12, '2016-02-27 17:17:25'),
    	(18, 9, '2016-01-27 17:17:25'),
    	(19, 3, '2016-02-27 17:17:25'),
    	(20, 3, '2016-01-27 17:17:25');
