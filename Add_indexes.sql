CREATE INDEX booking_index ON public."Deal_details"(booking_hours);
CREATE INDEX client_name_index ON public."Client" USING gin(to_tsvector('english', first_name || ' ' || last_name));


