﻿CREATE SEQUENCE parking_seq;
CREATE TABLE "Parking_space" (
	"number" integer NOT NULL DEFAULT nextval('parking_seq'),
	"type" varchar(10) NOT NULL,
	CONSTRAINT Parking_space_pk PRIMARY KEY ("number")
) WITH (
  OIDS=FALSE
);


CREATE SEQUENCE client_seq;
CREATE TABLE "Client" (
	"id" integer NOT NULL DEFAULT nextval('client_seq'),
	"first_name" varchar(50),
	"last_name" varchar(50) NOT NULL,
	"phone" varchar(15) NOT NULL,
	"car_number" varchar(10) NOT NULL,
	CONSTRAINT Client_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);


CREATE TABLE "Car" (
	"number" varchar(10) NOT NULL,
	"model_id" integer NOT NULL,
	CONSTRAINT Car_pk PRIMARY KEY ("number")
) WITH (
  OIDS=FALSE
);

CREATE SEQUENCE deal_seq;
CREATE TABLE "Deal" (
	"id" integer NOT NULL DEFAULT nextval('deal_seq'),
	"client_id" integer NOT NULL,
	"parking_number" integer NOT NULL,
	"emplyee_id" integer NOT NULL,
	CONSTRAINT Deal_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Parking_type" (
	"type" varchar(10) NOT NULL,
	"count_per_hour" FLOAT NOT NULL,
	CONSTRAINT Parking_type_pk PRIMARY KEY ("type")
) WITH (
  OIDS=FALSE
);


CREATE SEQUENCE model_seq;
CREATE TABLE "Model" (
	"id" integer NOT NULL DEFAULT nextval('model_seq'),
	"body_type" varchar(15) NOT NULL,
	"manufacter" varchar(15) NOT NULL,
	"model" varchar(15) NOT NULL,
	CONSTRAINT Model_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);


CREATE SEQUENCE empl_seq;
CREATE TABLE "Employee" (
	"id" integer NOT NULL DEFAULT nextval('empl_seq'),
	"first_name" varchar(50) NOT NULL,
	"last_name" varchar(50) NOT NULL,
	"position" varchar(10) NOT NULL,
	CONSTRAINT Employee_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);


CREATE TABLE "Deal_details" (
	"deal_id" integer NOT NULL,
	"booking_hours" integer NOT NULL,
	"begining_date" timestamp  NOT NULL,
	CONSTRAINT Deal_details_pk PRIMARY KEY ("deal_id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Position" (
	"title" varchar(10) NOT NULL,
	"salary" integer NOT NULL,
	CONSTRAINT Position_pk PRIMARY KEY ("title")
) WITH (
  OIDS=FALSE
);



ALTER TABLE "Parking_space" ADD CONSTRAINT "Parking_space_fk0" FOREIGN KEY ("type") REFERENCES "Parking_type"("type");

ALTER TABLE "Client" ADD CONSTRAINT "Client_fk0" FOREIGN KEY ("car_number") REFERENCES "Car"("number");

ALTER TABLE "Car" ADD CONSTRAINT "Car_fk0" FOREIGN KEY ("model_id") REFERENCES "Model"("id");

ALTER TABLE "Deal" ADD CONSTRAINT "Deal_fk0" FOREIGN KEY ("client_id") REFERENCES "Client"("id");
ALTER TABLE "Deal" ADD CONSTRAINT "Deal_fk1" FOREIGN KEY ("parking_number") REFERENCES "Parking_space"("number");
ALTER TABLE "Deal" ADD CONSTRAINT "Deal_fk2" FOREIGN KEY ("emplyee_id") REFERENCES "Employee"("id");

ALTER TABLE "Employee" ADD CONSTRAINT "Employee_fk0" FOREIGN KEY ("position") REFERENCES "Position"("title");

ALTER TABLE "Deal_details" ADD CONSTRAINT "Deal_details_fk0" FOREIGN KEY ("deal_id") REFERENCES "Deal"("id");

ALTER SEQUENCE parking_seq OWNED BY "Parking_space".number;
ALTER SEQUENCE client_seq OWNED BY "Client".id;
ALTER SEQUENCE deal_seq OWNED BY "Deal".id;
ALTER SEQUENCE model_seq OWNED BY "Model".id;
ALTER SEQUENCE empl_seq OWNED BY "Employee".id;
