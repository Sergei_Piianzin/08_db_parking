﻿SELECT 
  "Deal".parking_number, 
  "Client".car_number, 
  "Model".body_type, 
  "Model".manufacter, 
  "Model".model
FROM 
  public."Deal", 
  public."Car", 
  public."Client", 
  public."Model"
WHERE 
  "Deal".client_id = "Client".id AND
  "Car".model_id = "Model".id AND
  "Client".car_number = "Car"."number";
