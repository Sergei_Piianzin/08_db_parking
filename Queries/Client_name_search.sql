SELECT *
FROM public."Client"
WHERE to_tsvector('english', first_name || ' ' || last_name) @@ to_tsquery('Yoshi & Tannamuri');